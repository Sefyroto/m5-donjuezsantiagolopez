package Donjuez;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SangriaTest {

	@Test
	void test() { 
		assertEquals("Estic be", Sangria.gotsSangria(1));
		assertEquals("Estic be", Sangria.gotsSangria(2));
		assertEquals("Estic be", Sangria.gotsSangria(3));
		assertEquals("Me passat", Sangria.gotsSangria(4));
		assertEquals("Me passat", Sangria.gotsSangria(5));
	}
	
	@Test
	void test2() { 
		assertEquals("Estic be", Sangria.gotsSangria(-1));
		assertEquals("Me passat", Sangria.gotsSangria(1000));
	}
}
