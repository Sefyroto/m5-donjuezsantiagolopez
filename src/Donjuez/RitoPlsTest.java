package Donjuez;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RitoPlsTest {

	@Test
	void test() {
		assertEquals("350.0 3", RitoPls.todo(1000));
		assertEquals("500.0 5", RitoPls.todo(2500));
		assertEquals("573.5 6", RitoPls.todo(3235));
		assertEquals("628.1 7", RitoPls.todo(3781));
		assertEquals("694.4000000000001 7", RitoPls.todo(4444));
	}
	
	@Test
	void test2() {
		assertEquals("249.9 0", RitoPls.todo(-1));
		assertEquals("100250.0 10", RitoPls.todo(1000000));
	}
}
